var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

var x = 0, y = 0;

var snapshot; // store the image before drawing
var drawing = false; // cannot draw

// for redo undo
var shotid = 0;  // store the snapshot index after drawing;
var memory = []; // store the snapshot to redo & undo
memory[0] = ctx.getImageData(0, 0, canvas.width, canvas.height);

// initialize
var base = 4; // record the base linewidth
ctx.lineWidth = base;   
ctx.strokeStyle = '#000';
ctx.lineCap  = 'round';  
ctx.lineJoin = 'round'; 

// sizebox
var controling = false;
var sizebox  = document.getElementById("size-box");
var control  = document.getElementById("control");
var loading  = document.getElementById("loading");
var sizetext = document.getElementById("size-text");
// initialize the control css
control.style.left = 40 + "px";
sizetext.innerHTML="Size：" + 4;
loading.style.width = 40 + "px"; 
// function
control.addEventListener('mousedown', (e) => {
    controling = true;
    [x, y] = [e.offsetX, e.offsetY];
    
    sizebox.addEventListener('mousemove', (e) => {
        if(!controling) return;
        //cannot use e.offsetX since it would also catch others position
        control.style.left = e.clientX-sizebox.offsetLeft-x + "px"; // change the css 
        if(e.clientX-sizebox.offsetLeft-x <= 0)   control.style.left = "0px";
        if(e.clientX-sizebox.offsetLeft-x >= 200) control.style.left = "200px";
        if(Math.floor(control.offsetLeft / 10) === 0) sizetext.innerHTML="Size：1";
        else sizetext.innerHTML="Size：" + Math.floor(control.offsetLeft / 10);
		loading.style.width = control.offsetLeft + "px"; // loading css
    });
    
    document.addEventListener('mouseup', () => {
        controling = false;
        if(Math.floor(control.offsetLeft / 10) === 0) [base, ctx.lineWidth] = [1, 1];
        else base = ctx.lineWidth = Math.floor(control.offsetLeft / 10);
    });
});

// color
var color;
function bp(){ ctx.strokeStyle = ctx.fillStyle = '#8a2be2'};
function oy(){ ctx.strokeStyle = ctx.fillStyle = '#ffae42'};
function pur(){ ctx.strokeStyle = ctx.fillStyle = 'purple'};
function yel(){ ctx.strokeStyle = ctx.fillStyle = 'yellow'};
function pr(){ ctx.strokeStyle = ctx.fillStyle = '#800040'};
function yg(){ ctx.strokeStyle = ctx.fillStyle = '#9acd32'};
function red(){ ctx.strokeStyle = ctx.fillStyle = 'red'};
function gre(){ ctx.strokeStyle = ctx.fillStyle = 'green'};
function ro(){ ctx.strokeStyle = ctx.fillStyle = '#ff4500'};
function gb(){ ctx.strokeStyle = ctx.fillStyle = '#11b4b3'};
function ora(){ ctx.strokeStyle = ctx.fillStyle = 'orange'};
function blu(){ ctx.strokeStyle = ctx.fillStyle = 'blue'};

// rainbow
let hue = 0;
var time = 0;
var rainbowcolor = false;
var rainbow = document.getElementById("rainbow");
rainbow.addEventListener('click', () => {
    time ++;

    if((time % 2) === 0){
        rainbowcolor = false;
        ctx.strokeStyle = '#000';
    } 
    else {
        alert('click again to turn off the rainbow mode');
        rainbowcolor = true;
    }
});
function dorainbow(){
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    hue++;
    if (hue >= 360) hue = 0;
};

// pen
var pen = document.getElementById("pen");
var mode = pen; // initial mode
pen.addEventListener('click', () => {
    mode = pen;
    ctx.shadowBlur = 0;
    ctx.strokeStyle = '#000';
});

// eraser
var eraser = document.getElementById("eraser");
eraser.addEventListener('click', () => {
    mode = eraser;
    ctx.shadowBlur = 0;
    ctx.strokeStyle = '#fff'; 
});

// shadowpen
var shadowpen = document.getElementById("shadow");
shadowpen.addEventListener('click', () => {
    mode = shadowpen
    ctx.shadowBlur = 10;
    ctx.shadowColor = 'rgb(0, 0, 0)';
    ctx.strokeStyle = '#000';
});

// pencil
var pencil = document.getElementById("pencil");
pencil.addEventListener('click', () => {
    mode = pencil
    ctx.shadowBlur = 0;
    ctx.strokeStyle = '#000';
});

// sliced
var sliced = document.getElementById("sliced");
sliced.addEventListener('click', () => {
    mode = sliced;
    ctx.shadowBlur = 0;
    ctx.strokeStyle = '#000';
});

// line
var line = document.getElementById("lin");
line.addEventListener('click', () => {
    mode = line;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = '#000';
    canvas.style.cursor = 'crosshair';
});

// circle
var circle = document.getElementById("cir");
circle.addEventListener('click', () => {
    mode = circle;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = '#000';
    canvas.style.cursor = 'crosshair';
});

// triangle
var tri = document.getElementById("tri");
tri.addEventListener('click', () => {
    mode = tri;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = '#000';
    canvas.style.cursor = 'crosshair';
});

// rectangle
var rec = document.getElementById("rec");
rec.addEventListener('click', () => {
    mode = rec;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = '#000';
    canvas.style.cursor = 'crosshair';
});

// download
function download(){
    var download = document.getElementById("download");
    download.href = canvas.toDataURL();
};

// upload
var upload = document.getElementById('upload');
upload.addEventListener('change', handleImage, false);
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(e) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            ctx.lineWidth = radius*2;
            push();
            url = canvas.toDataURL();
        }
        img.src = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

// redo
var redo = document.getElementById("redo");
redo.addEventListener('click', () => {
    ctx.putImageData(memory[shotid+1], 0, 0);
    shotid++;
});

// undo
var undo = document.getElementById("undo");
undo.addEventListener('click', () => {
    ctx.putImageData(memory[shotid-1], 0, 0);
    shotid--;
});

// clean
var clean = document.getElementById("clean");
clean.addEventListener('click', () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    shotid = 0;  // restore the snapshot index after drawing
    memory.length = 0; // restore the snapshot to redo & undo
    memory[0] = ctx.getImageData(0, 0, canvas.width, canvas.height);
});

// text
var size = 3;
var text = document.getElementById("text");
text.addEventListener('click', () => {
    mode = text
    canvas.style.cursor = 'text';
});
var box = false; // use to check whether there is already a textbox
function addbox(e){
    var textbox = document.createElement('input');
    // set textbox's html & css 
    textbox.type  = 'text';
    textbox.id = 'textbox';
    textbox.style.position = 'fixed';
    textbox.style.left = e.clientX + 'px';
    textbox.style.top  = e.clientY + 'px';
    textbox.style.height = size*10 + 'px';
    textbox.canvasplaceX = e.offsetX;
    textbox.canvasplaceY = e.offsetY;
    textbox.style.border = '1px dashed';
    // the events of textbox
    textbox.onkeydown = enter; 
    // add it to the body
    document.body.appendChild(textbox);
};
function enter(e){
    if(e.keyCode === 13){
        ctx.fillText(this.value, this.canvasplaceX, this.canvasplaceY + size*10);
        document.body.removeChild(this);
        box = false;
        // store image to redo or undo
        shotid++;
        memory[shotid] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    }
};
// text size
var showsize = document.getElementById('showsize');
var sizeup   = document.getElementById('sizeup');
var sizedown = document.getElementById('sizedown');
showsize.innerHTML = size;
sizeup.addEventListener('click', () => {
    if(size < 10) size++;
    showsize.innerHTML = size;
});
sizedown.addEventListener('click', () => {
    if(size > 1) size--;
    showsize.innerHTML = size;
});


// mousedown
canvas.addEventListener('mousedown', (e) => {
    if(mode === text){
        if(box){
            var textbox = document.getElementById('textbox');
            document.body.removeChild(textbox);
        } else box = true;

        if(document.getElementById('style').checked === true) ctx.font = size*10 + "px fantasy";
        else ctx.font = size*10 + "px Arial";
        addbox(e); // add the textbox

    } else {
        
        if(box){
            var textbox = document.getElementById('textbox');
            document.body.removeChild(textbox);
            box = false;
        }
        drawing = true; // can draw
        snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height); // store the image before drawing
        [x, y] = [e.offsetX, e.offsetY]; // store the initial position before drawing
    }
});

// mousemove
canvas.addEventListener('mousemove', (e) => {
    if(!drawing) return;
    
    ctx.beginPath(); // ready to draw new path
    
    if(rainbowcolor === true && mode != eraser) dorainbow();

    if(mode === pencil) ctx.lineWidth = Math.floor(Math.random()*3 + base); // set pencil width
    else ctx.lineWidth = base;

    // pen , eraser , shadowpen , pencil draw 
    if(mode === pen || mode === eraser || mode === shadowpen || mode === pencil){
        ctx.moveTo(x, y); // begin from
        ctx.lineTo(e.offsetX, e.offsetY); // end to
        ctx.stroke(); // draw
        [x, y] = [e.offsetX, e.offsetY];
    }

    // sliced
    if(mode === sliced){
        ctx.moveTo(x, y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    
        ctx.moveTo(x - 4, y - 4);
        ctx.lineTo(e.offsetX - 4, e.offsetY - 4);
        ctx.stroke();
    
        ctx.moveTo(x - 2, y - 2);
        ctx.lineTo(e.offsetX - 2, e.offsetY - 2);
        ctx.stroke();
    
        ctx.moveTo(x + 2, y + 2);
        ctx.lineTo(e.offsetX + 2, e.offsetY + 2);
        ctx.stroke();
    
        ctx.moveTo(x + 4, y + 4);
        ctx.lineTo(e.offsetX + 4, e.offsetY + 4);
        ctx.stroke();
    
        [x, y] = [e.offsetX, e.offsetY];
    }

    // line
    if(mode === line){
        ctx.putImageData(snapshot, 0, 0);
        ctx.moveTo(x, y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.closePath();
        ctx.stroke();
    }

    // circle
    if(mode === circle){
        ctx.putImageData(snapshot, 0, 0);
        var radius = (Math.sqrt((Math.pow((e.offsetX - x), 2) + Math.pow((e.offsetY - y), 2)))) / 2;
        ctx.arc((e.offsetX + x)/2, (e.offsetY + y)/2, radius, 0, 2 * Math.PI,);
        if(document.getElementById("fill").checked === true) ctx.fill();
        else ctx.stroke();        
    }

    // rectangle
    if(mode === rec){
        ctx.putImageData(snapshot, 0, 0);
        if(document.getElementById("fill").checked === true) 
            ctx.fillRect(x, y, e.offsetX - x, e.offsetY - y);
        else ctx.strokeRect(x, y, e.offsetX - x, e.offsetY - y);
    }

    // triangle
    if(mode === tri){
        ctx.putImageData(snapshot, 0, 0);
        ctx.moveTo(x, y);
        ctx.lineTo(x, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.closePath();
        if(document.getElementById("fill").checked === true) ctx.fill();
        else ctx.stroke();
    }

});

// mouseup & mouseout
canvas.addEventListener('mouseup', () => {
    
    if(drawing === true){
        shotid++;
        memory[shotid] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    }
    drawing = false;
});

canvas.addEventListener('mouseout', () => {

    if(drawing === true){
        shotid++;
        memory[shotid] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    }
    drawing = false;
});




