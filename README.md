# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

The initial mode would be 'pen'.

On the right side :
1. pen : brush,  click to draw(initial mode would already be pen).
2. era : eraser, click and draw on the canvas to erase .
3. sha : shadowpen, click to draw, the line would be drawn with shadow.
4. pcl : pencil, click to draw, the line's width would be random in small range
    (if the size is small, the outcome would be more obvious).
5. sli : sliced pen, click to draw, the line would be sliced in some angle.

6. size : drag the button to control the size of the line you want to draw.

7. redo/undo/clean : as its name.

8.  lin : click to draw line.
9.  cir : click to draw circle.
10. rec : click to draw rectangle.
11. tri : click to draw triangle.
! the cursor would change after click the shape (shown on canvas)!

12. fill the shape : if clicked, the shape you draw would be filled.

On the left side :

1. color wheel : click to change the color
2. rainbow : click to change to the rainbow mode, the color you draw would change continuously
    (click again to close the mode)
3. text : click to change to text mode, then click any place on the canvas and type the text in,
    then press enter to draw the text on the canvas.(if the former text hasn't been finished and you just click
    other places on the canvas, the former would be removed).
! the cursor would change after click the shape (shown on canvas)!
4. text size : click to change the text size.
5. change the font : click to change the text font.(origin font is "arial", after change is "fantasty").
6. download/upload : as its name(to upload just click the word).

other tips: you can choose shadowpen then choose any shapes(circle, rec, tri) to draw the shadow shapes.


